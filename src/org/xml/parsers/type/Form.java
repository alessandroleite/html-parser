package org.xml.parsers.type;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Form extends TybeBase 
{
	private String action;
	private String method;

	private final List<Type> elements = new ArrayList<Type>();

	public Form() {
		this.setType(Form.class.getSimpleName().toLowerCase());
	}

	public boolean addElement(Type type) {
		if (type != null) {
			return this.elements.add(type);
		}
		return false;
	}

	public boolean removeElement(Type type) {
		return this.elements.remove(type);
	}

	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @param action
	 *            the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * @return the method
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * @param method
	 *            the method to set
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * @return the elements
	 */
	public List<Type> getElements() {
		return Collections.unmodifiableList(elements);
	}

	@Override
	public String toString() 
	{
		StringBuilder sb = new StringBuilder(String.format("type:%s,id=%s,action=%s,method=%s\n", this.getType(), nullAsEmpty(this.getId()),
				nullAsEmpty(this.getAction()), nullAsEmpty(this.getMethod())));

		for (Type type : this.elements) 
		{
			sb.append('\t').append(type.toString()).append('\n');
		}

		return sb.toString();
	}
}