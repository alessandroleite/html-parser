package org.xml.parsers.type;

public interface Type 
{
	String getId();
	String getName();
	String getType();
}