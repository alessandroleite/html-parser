package org.xml.parsers.type;

public abstract class TybeBase implements Type {
	private String name;
	private String id;
	private String type;

	public TybeBase() {
		super();
	}

	public TybeBase(String id, String name) {
		this.id = id;
		this.name = name;
	}

	public TybeBase(String id, String name, String type) {
		this.id = id;
		this.name = name;
		this.type = type;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getName() {
		return this.name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	protected String nullAsEmpty(String value) {
		return value == null ? "" : value;
	}

	@Override
	public String toString() {
		return String.format("type:%s id:%s name:%s",
				nullAsEmpty(this.getType()), nullAsEmpty(this.getId()),
				nullAsEmpty(this.getName()));
	}
}