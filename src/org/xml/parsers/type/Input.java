package org.xml.parsers.type;
public class Input extends TybeBase {
		private String value;

		public Input() {
			super();
		}

		public Input(String name, String id) {
			super(name, id);
		}

		public Input(String name, String id, String value) {
			super(name, id);
			this.value = value;
		}

		/**
		 * @return the value
		 */
		public String getValue() {
			return value;
		}

		/**
		 * @param value
		 *            the value to set
		 */
		public void setValue(String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			String base = super.toString();
			return String.format("%s value = %s", base, value);
		}
	}