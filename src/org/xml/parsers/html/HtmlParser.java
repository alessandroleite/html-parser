package org.xml.parsers.html;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.parsers.DocumentBuilderFactory.FactoryType;
import org.xml.parsers.type.Form;
import org.xml.parsers.type.Input;
import org.xml.sax.SAXException;

public class HtmlParser 
{
	
	public static Form parser(String html) throws ParserConfigurationException, SAXException, IOException 
	{
		if (html == null) 
		{
			throw new NullPointerException();
		}

		final Form f = new Form();

		if (!html.trim().isEmpty()) 
		{
			//Document document = org.xml.parsers.DocumentBuilderFactory.getInstance(FactoryType.DOM).parser(html);
			Document document = org.xml.parsers.DocumentBuilderFactory.getInstance(FactoryType.TIDY).parser(html);
			
			Element root = document.getDocumentElement();
			NodeList form = root.getElementsByTagName("form");

			if (form != null) 
			{
				for (int i = 0; i < form.getLength(); i++) 
				{
					Node item = form.item(i);

					if (item instanceof Element) 
					{
						f.setMethod(((Element) item).getAttribute("method"));
						f.setAction(((Element) item).getAttribute("action"));
						f.setName(((Element) item).getAttribute("name"));

						NodeList formElements = item.getChildNodes();

						for (int j = 0; j < formElements.getLength(); j++) 
						{
							Node formElement = formElements.item(j);
							if (formElement instanceof Element) 
							{
								Element el = (Element) formElement;
								Input input = new Input(el.getAttribute("name"), el.getAttribute("id"));
								
								input.setValue(el.getAttribute("value"));
								input.setType(el.getAttribute("type"));
								f.addElement(input);
							}
						}
					}
				}
			}
		}
		return f;
	}

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException 
	{
		File f = new File("/home/alessandro/workspace/xmldom/form.html");

		Form form = parser(read(f));
		System.err.println(form);
	}

	static String read(File f) throws IOException 
	{
		BufferedReader reader = null;
		StringBuilder sb = new StringBuilder();
		
		try 
		{
			reader = new BufferedReader(new FileReader(f));
			int i;

			while ((i = reader.read()) != -1) 
			{
				sb.append((char) i);
			}
		} finally 
		{
			if (reader != null) 
			{
				reader.close();
			}
		}
		
		return sb.toString();
	}
}
