package org.xml.parsers;

import java.io.IOException;

import org.w3c.dom.Document;

public abstract class DocumentBuilderFactory 
{
	public static enum FactoryType
	{
		TIDY("org.xml.parsers.impl.JTidyDocumentBuilderFactory"),
		
		DOM("org.xml.parsers.impl.DOMDocumentFactory");
		
		private String factoryClass;
		
		private FactoryType(String clazz)
		{
			this.factoryClass = clazz;
		}
		
		public String getFactoryClass() 
		{
			return factoryClass;
		}
	}
	
	
	public abstract Document parser(String value) throws IOException;
	
	public static DocumentBuilderFactory getInstance(FactoryType type)
	{
		try 
		{
			return (DocumentBuilderFactory) Class.forName(type.getFactoryClass()).newInstance();
		} 
		catch (InstantiationException e) 
		{
			throw new RuntimeException(e.getMessage(), e);
		} 
		catch (IllegalAccessException e) 
		{
			throw new RuntimeException(e.getMessage(), e);
		} 
		catch (ClassNotFoundException e) 
		{
			throw new RuntimeException(e.getMessage(), e);
		}
	}
}
