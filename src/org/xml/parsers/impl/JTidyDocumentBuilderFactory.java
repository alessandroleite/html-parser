package org.xml.parsers.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.w3c.dom.Document;
import org.w3c.tidy.Tidy;
import org.xml.parsers.DocumentBuilderFactory;

public class JTidyDocumentBuilderFactory extends DocumentBuilderFactory 
{
	@Override
	public Document parser(String value) throws IOException 
	{
		Tidy tidy = new Tidy();
		tidy.setFixComments(true);
		tidy.setFixBackslash(true);
		tidy.setFixUri(true);
		tidy.setForceOutput(true);

		ByteArrayInputStream bais = null;

		try 
		{
			return tidy.parseDOM(bais = new ByteArrayInputStream(value.getBytes()), (OutputStream) null);
		} 
		finally 
		{
			if (bais != null)
			{
				bais.close();
			}
		}
	}
}
