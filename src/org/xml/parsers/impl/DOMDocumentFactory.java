package org.xml.parsers.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.parsers.DocumentBuilderFactory;
import org.xml.sax.SAXException;

public class DOMDocumentFactory extends DocumentBuilderFactory 
{
	@Override
	public Document parser(final String value) throws IOException 
	{
		javax.xml.parsers.DocumentBuilderFactory bf = javax.xml.parsers.DocumentBuilderFactory.newInstance();
		ByteArrayInputStream is = null;
		
		try
		{
			DocumentBuilder builder = bf.newDocumentBuilder();
			is = new ByteArrayInputStream(value.getBytes());
			return builder.parse(is);
			
		}catch(SAXException exception)
		{
			throw new IOException(exception.getMessage(), exception);
		}catch(ParserConfigurationException exception)
		{
			throw new IOException(exception.getMessage(), exception);
		}
		finally
		{
			if (is != null)
			{
				is.close();
			}
		}
	}
}
